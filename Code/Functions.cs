﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LV3_LanguageMachine
{
    public class LangWorker {
        string input;
        List<string> SplittedInput;
        List<KeyValuePair<string, string>> SyntaxIdentifier;
        Dictionary<string, int> SyntaxCounter;
        Dictionary<string, string> RegexPatterns;

        public LangWorker(string input) {
            this.input = input;
            this.SplittedInput = new List<string>();
            this.SyntaxIdentifier = new List<KeyValuePair<string, string>>();
            this.SyntaxCounter = new Dictionary<string, int>();
            this.RegexPatterns = new Dictionary<string, string>();

            initializeRegexPaterns();
        }

        public void checkNextDigitTest() {
            splitString();
            sortByType();
        }

        private void splitString() {
            string tempString = "";
            for(int i = 0; i < input.Length; i++) {
                if (Char.IsLetterOrDigit(input[i])) {
                    tempString = tempString + input[i];
                } else {
                    if (tempString != "") {
                        SplittedInput.Add(tempString);
                        tempString = "";
                    }
                    SplittedInput.Add(input[i].ToString());
                }
            }
            if (tempString != "") {
                SplittedInput.Add(tempString);
            }
        }

        private void sortByType() {
            foreach (string syntaxMember in SplittedInput) {
                foreach (KeyValuePair<string, string> regexPattern in RegexPatterns) {
                    Match match = Regex.Match(syntaxMember, regexPattern.Value);
                    if (match.Success) {
                        SyntaxIdentifier.Add(new KeyValuePair<string, string>(syntaxMember, regexPattern.Key));
                        if (SyntaxCounter.ContainsKey(regexPattern.Key)) {
                            SyntaxCounter[regexPattern.Key] += 1;
                        } else {
                            SyntaxCounter.Add(regexPattern.Key, 1);
                        }
                    }
                }
            }
        }

        public void exportToTXTFile(string path) {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(path, append: true)) {
                if (MyStaticValues.ExportCounterLangWorker == 0) {
                    MyStaticValues.ExportCounterLangWorker = 1;
                } else {
                    MyStaticValues.ExportCounterLangWorker++;
                }
                file.WriteLine("Print line " + MyStaticValues.ExportCounterLangWorker + ": " + input);
                foreach (string line in SplittedInput) {
                    file.WriteLine(line);
                }
                file.WriteLine("-----------------------------------------------------");
            }
        }

        private void initializeRegexPaterns() {
            string Identificator = @"\A[a-zA-Z]+[a-zA-Z0-9]*\z";
            string Separator = @"\A[,\ ]\z";
            string Operator = @"\A[+\-=]\z";
            string Comment = @"\A[\\]{2}\z";
            string Constant = @"\A[\d]+\z";
            //string name = @"\A[]\z"; 
            //RegexPatterns.Add(nameof(name), name);
            RegexPatterns.Add(nameof(Identificator), Identificator);
            RegexPatterns.Add(nameof(Separator), Separator);
            RegexPatterns.Add(nameof(Operator), Operator);
            RegexPatterns.Add(nameof(Comment), Comment);
            RegexPatterns.Add(nameof(Constant), Constant);
        }

        public Dictionary<string, int> getSyntaxCounter() {
            return this.SyntaxCounter;
        }

        public List<KeyValuePair<string, string>> getSyntaxIdentifier() {
            return SyntaxIdentifier;
        }
    }

    public static class MyStaticValues {
        public static int ExportCounterLangWorker { get; set; }
        public static string pathToExport { get; set; }
    }
}